//
//  Constants.swift
//  WeatherApp
//
//  Created by Paul Lascar on 20/07/2017.
//  Copyright © 2017 Paul Lascar. All rights reserved.
//

import Foundation


let BASE_URL = "http://api.openweathermap.org/data/2.5/weather?"

let LATITUDE = "lat="

let LONGITUTDE = "&lon="

let APP_ID = "&appid="

let API_KEY = "065e154ff22f0b8cc0941743d216f6e0"

typealias DownloadComplete = () -> ()

let CURRENT_WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather?lat=\(Location.sharedInstance.latitude!)&lon=\(Location.sharedInstance.longitude!)&appid=065e154ff22f0b8cc0941743d216f6e0"
let FORECAST_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?lat=\(Location.sharedInstance.latitude!)&lon=\(Location.sharedInstance.longitude!)&cnt=10&mode=json&appid=065e154ff22f0b8cc0941743d216f6e0"

