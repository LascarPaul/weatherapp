//
//  Location.swift
//  WeatherApp
//
//  Created by Paul Lascar on 23/07/2017.
//  Copyright © 2017 Paul Lascar. All rights reserved.
//

import CoreLocation

class Location {
    static var sharedInstance = Location()
    private init() {}
    
    var latitude: Double!
    var longitude: Double!
}
