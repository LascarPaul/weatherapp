//
//  WeatherCell.swift
//  WeatherApp
//
//  Created by Paul Lascar on 23/07/2017.
//  Copyright © 2017 Paul Lascar. All rights reserved.
//

import UIKit
@IBDesignable
class WeatherCell: UITableViewCell {

    @IBOutlet weak var lowTempLbl: UILabel!
    @IBOutlet weak var highTempLbl: UILabel!
    @IBOutlet weak var weatherInfoLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var weatherIconView: UIImageView!
    
    func configureCell(forecast: Forecast) {
        lowTempLbl.text = "\(forecast.lowTemp)C°"
        highTempLbl.text = "\(forecast.highTemp)C°"
        weatherInfoLbl.text = forecast.weatherInfo
        dateLbl.text = forecast.date
        weatherIconView.image = UIImage(named: forecast.weatherInfo)
    }
    
    @IBInspectable var cornerRadius: CGFloat = 5
    
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var shadowColor: UIColor? = UIColor.black
    @IBInspectable var shadowOpacity: Float = 0.5
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        
        layer.cornerRadius = 10
        layer.masksToBounds = false
        layer.shadowColor = shadowColor?.cgColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }


    

}
