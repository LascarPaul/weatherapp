//
//  CurrentWeather.swift
//  WeatherApp
//
//  Created by Paul Lascar on 20/07/2017.
//  Copyright © 2017 Paul Lascar. All rights reserved.
//

import UIKit
import Alamofire

class CurrentWeather {
    var _cityName: String!
    var _date: String!
    var _weatherInfo: String!
    var _currentTemp: Double!
    
    var cityName: String {
        if _cityName == nil {
            _cityName = ""
        }
        return _cityName
    }
    
    var date: String {
        if _date == nil {
            _date = ""
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
        let currentDate = dateFormatter.string(from: Date())
        self._date = "Today, \(currentDate)"
        return _date
    }
    
    var weatherInfo: String {
        if _weatherInfo == nil {
            _weatherInfo = ""
        }
        return _weatherInfo
    }
    
    var currentTemp: Double {
        if _currentTemp == nil {
            _currentTemp = 0.0
        }
        return _currentTemp
    }
    
    
    func downloadWeatherDetails(completed: @escaping DownloadComplete) {
        let currentWeatherUrl = URL(string: CURRENT_WEATHER_URL)!
        Alamofire.request(currentWeatherUrl).responseJSON { response in
            let result = response.result
            
            if let dict = result.value as? Dictionary<String, AnyObject> {
                
                if let name = dict["name"] as? String {
                    self._cityName = name.capitalized
                    
                    
                }
                
                if let weather = dict["weather"] as? [Dictionary <String, AnyObject>] {
                    if let main = weather[0]["main"] as? String {
                        self._weatherInfo = main
                        
                    }
                }
                
                if let main = dict["main"] as? Dictionary <String, AnyObject> {
                    if let temp = main["temp"] as? Double {
                        let celsiusTemp = Double(round(temp - 273.15))
                        self._currentTemp = celsiusTemp
                        
                        
                        
                    }
                }
            }
            
            completed()
        }
        
    }
    
}

