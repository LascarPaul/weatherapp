//
//  Forecast.swift
//  WeatherApp
//
//  Created by Paul Lascar on 20/07/2017.
//  Copyright © 2017 Paul Lascar. All rights reserved.
//

import UIKit
import Alamofire

class Forecast {
    private var _date: String!
    private var _weatherInfo: String!
    private var _highTemp: String!
    private var _lowTemp: String!
    private var forecast = [Forecast]()
    
    var date: String {
        if _date == nil {
            _date = ""
        }
        return _date
    }
    
    var weatherInfo: String {
        if _weatherInfo == nil {
            _weatherInfo = ""
        }
        return _weatherInfo
    }
    
    var highTemp: String {
        if _highTemp == nil {
            _highTemp = ""
        }
        return _highTemp
    }
    
    var lowTemp: String {
        if _lowTemp == nil {
            _lowTemp = ""
        }
        return _lowTemp
    }
    
    
    init(weatherDict: Dictionary<String, AnyObject>) {
        
        if let temp = weatherDict["temp"] as? Dictionary<String, AnyObject> {
            if let min = temp["min"] as? Double {
                let celsiusTemp = Double(round(min - 273.15))
                self._lowTemp = "\(celsiusTemp)"
            }
            
            
            if let max = temp["max"] as? Double {
                let celsiusTemp = Double(round(max - 273.15))
                self._highTemp = "\(celsiusTemp)"
                
            }
            
            
        }
        if let weather = weatherDict["weather"] as? [Dictionary<String, AnyObject>] {
            if let main = weather[0]["main"] as? String {
                self._weatherInfo = main
            }
        }
        
        if let date = weatherDict["dt"] as? Double {
            
            let unixConvertedDate = Date(timeIntervalSince1970: date)
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .full
            dateFormatter.dateFormat = "EEEE"
            dateFormatter.timeStyle = .none
            self._date = unixConvertedDate.dayOfTheWeek()
        }
    }
    
}

extension Date {
    func dayOfTheWeek() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self)
    }
}
